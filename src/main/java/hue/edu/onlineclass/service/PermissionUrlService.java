package hue.edu.onlineclass.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.mapper.PermissionUrlMapper;
import hue.edu.onlineclass.model.PermissionUrl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 17670
 */
@Service
public class PermissionUrlService {
    /***
     * permission_url的管理：
     * uuid的增删改查
     * url的增删改查
     *
     */
    @Resource
    private PermissionUrlMapper permissionUrlMapper;

    /***
     * 通过id查看详情
     */
    public Page<PermissionUrl> getinfo(PermissionUrl permissionUrl, Page<PermissionUrl> page){
        QueryWrapper<PermissionUrl> wapper = new QueryWrapper<>();
        wapper.eq("permission_uuid", "%"+permissionUrl.getPermission_uuid()+"%");
        Page permissionpage = permissionUrlMapper.selectPage(page, wapper);
        return permissionpage;
    }


    /***
     * 通过id更改或增加
     * @return
     */
    public Integer addPermissionUrl(PermissionUrl permissionUrl){
        Integer permission;
        if      (permissionUrl.getId() != null){
            permission = permissionUrlMapper.updateById(permissionUrl);
        }
        else{
            permission = permissionUrlMapper.insert(permissionUrl);
        }
        return permission;
    }

    /**
     * 通过id查询
     */
    public PermissionUrl getInfo(Integer id){
        return permissionUrlMapper.selectById(id);
    }
    /***
     * 通过id删除
     */
    public Integer delPermissionUrl(Integer id){
        return permissionUrlMapper.deleteById(id);
    }
}
