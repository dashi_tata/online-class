package hue.edu.onlineclass.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author liangzhu
 */
@Data
@TableName("tb_permission")
public class  Permission {
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 权限名
     */
    @TableField("permission")
    private String permission;
    /**
     * url
     */
    @TableField("url")
    private String url;
}
