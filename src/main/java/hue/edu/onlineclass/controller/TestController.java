package hue.edu.onlineclass.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

//@Controller
public class TestController {
    /**
     * 测试页面
     */
    @RequestMapping("/test/list")
    public String testIndex(){
        return "test/test-list";
    }
    @RequestMapping("/test/add")
    public String testAdd(){
        return "test/test-add";
    }
    @RequestMapping("/testschool/add")
    public String testAdd1(){
        return "school/add";
    }
    @RequestMapping("/test/coursefilelist")
    public String coursefileIndex(){
        return "test/coursefile-list";
    }

    @RequestMapping("/test/coursefileadd")
    public String coursefileaddIndex(){
        return "test/coursefile-add";
    }

    @RequestMapping("/test/userset")
    public String usersetIndex(){
        return "test/userset";
    }

    @RequestMapping("/test/userdetail")
    public String userdetailIndex(){
        return "test/userdetail";
    }

    @RequestMapping("/school/index")
    public String schoolFileaddIndex(){
        return "school/index";
    }
}
