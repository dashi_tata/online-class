package hue.edu.onlineclass.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.mapper.PermissionMapper;
import hue.edu.onlineclass.model.Permission;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author liunian
 */
@Service
public class PermissionService {

    @Resource
    private PermissionMapper permissionMapper;

    /***
     * 权限管理：
     * 权限名称的增加
     * 权限名称的删除
     * 权限名称的更改
     * 权限名称的查询
     */


    public Page<Permission> getList(Permission permission,Page<Permission> page){
        // 模糊搜索
        QueryWrapper<Permission> wrapper = new QueryWrapper<>();
        if (Strings.isNotBlank(permission.getPermission())){
            wrapper.eq("name","%"+permission.getPermission()+"%");
        }
        return permissionMapper.selectPage(page, wrapper);
    }


    /**
     * 权限名称的增加和修改
     */
    public Integer addOrEditPermission(Permission permission){
        Integer effectLine;
        if (permission.getId()!=null){
            // 更新
            effectLine = permissionMapper.updateById(permission);
        }else {
            // 新增
            effectLine = permissionMapper.insert(permission);
        }
        return effectLine;
    }

    /**
     *权限名称的删除
     */
    public Integer delPermission(Integer id){
        return permissionMapper.deleteById(id);
    }

    /**
     *权限名称的查询
     */
    public Permission getPermissionInfo(Integer id){return  permissionMapper.selectById(id);}




}
