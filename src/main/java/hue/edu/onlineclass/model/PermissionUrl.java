package hue.edu.onlineclass.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 梁柱
 */
@Data
@TableName("tb_permission_url")
@Deprecated
public class PermissionUrl {
    /**
     * 即将弃用
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    @TableField("permission_uuid")
    private String permission_uuid;
    @TableField("url")
    private String url;
}
