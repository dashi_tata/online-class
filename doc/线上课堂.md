# 线上课堂

## 数据库

#### 用户表`tb_user`

##### 字段:

id, 用户名,密码,姓名,性别,地址,邮箱,权限,状态

|                 |        |                        |
| :-------------: | :----: | :--------------------: |
|       id        |  主键  |       必填,自增        |
|    username     | 用户名 |     必填,varchar()     |
|    password     |  密码  | 必填,md5加密,varchar() |
|      name       |  姓名  |       varchar()        |
|       sex       |  性别  |       tinyint(1)       |
|     address     |  地址  |      varchar(225)      |
|      email      |  邮箱  |      varchar(32)       |
| permission_uuid | 权限码 |      varcahr(32)       |
|     status      |  状态  |       tinyint(1)       |



#### 权限表`tb_permission`

id,权限名,权限码(uuid)

|            |        |                       |
| ---------- | ------ | --------------------- |
| id         | 主键   | 必填,主键,自增        |
| name       | 权限名 | varchar(32)(不可重复) |
| uuid(取消) | 权限码 | varchar(32)           |



#### 权限对应表`tb_permission_url`

id,权限码,url;

|                 |          |              |
| --------------- | -------- | ------------ |
| id              | 主键     | 必填,自增    |
| permission_name | 权限名   | varchar(32)  |
| url             | 访问路径 | varchar(128) |



#### 学校表`tb_school`

id,学校名,学校地址

|         |          |                |
| ------- | -------- | -------------- |
| id      | id       | 必填,主键,自增 |
| name    | 学校名   | varchar(32)    |
| address | 学校地址 | varchar(225)   |
| info    | 学校信息 | varchar(225)   |



#### 课程表`tb_course`

id,课程号,课程名,课程类型,授课老师,课程信息

|               |          |              |
| ------------- | -------- | ------------ |
| id            | id       | 主键 uuid    |
| name          | 课程名   | varchar(128) |
| type          | 课程类型 | varchar(16)  |
| tearcher_name | 授课老师 | varchar(32)  |
| info          | 课程信息 | varchar(32)  |



#### 课程视频表`tb_course_video`

id,视频文件名,视频地址,视频点击量,课程号

|            |          |                      |
| ---------- | -------- | -------------------- |
| id         | 主键     | 主键,自增            |
| name       | 视频名   | varchar(128)         |
| path       | 本地路径 | varchar(128)         |
| click_num  | 点击量   | 初始化为零(访问自增) |
| course_num | 课程号   | 外键(课程表)         |



#### 课程附件表`tb_course_file`

id,附件文件名,附件地址,附件下载量,课程号

|            |          |                      |
| ---------- | -------- | -------------------- |
| id         | 主键     | 主键,自增            |
| name       | 视频名   | varchar(128)         |
| path       | 本地路径 | varchar(128)         |
| click_num  | 点击量   | 初始化为零(访问自增) |
| course_num | 课程号   | 外键(课程表)         |



## 模块设计

### 学习者端功能模块

1. 用户模块：用户注册，登录，修改个人信息，修改密码，退出。

2. 课程模块：热门课程显示，推荐课程显示，按分类查询课程，按条件模糊查询课程，查看课程详细内容

3. 课程选修模块：参加课程，退选课程，订单提交，取消订单

4. 课件使用模块：观看视频，自主选择视频

5. 公告通知模块：查看系统通知公告，根据所选课程查看公告\[`不重要`]

6. 反馈模块：提出反馈意见，查看回复\[`不重要`]

7. 讨论模块：根据每一课时发表评论和问题，对相应问题进行回复\[`不重要`]

### 教学者功能模块

1. 用户模块：用户注册，用户登录，修改个人信息，退出

2. 课程管理模块：添加课程，编辑课程基本信息，删除课程，查看自己所开设课程

3. 课件管理模块：根据课程查看对应课程下的章节，根据章节查看对应课时，发布章节，发布课时

4. 公告通知模块：发布对应课程的公告，删除公告\[`不重要`]

5. 反馈模块：发表反馈意见\[`不重要`]

6. 讨论模块：查看评价，发表问题，对问题发表回复\[`不重要`]

### 后台管理员功能模块

1. 管理员模块：管理员登录与退出，密码修改

2. 新闻公告模块:发布公告，查看公告

3. 教师审核模块：审核教师身份，查看所有教师

4. 课程类别模块：查看课程类别，删除课程类别

5. 反馈模块：对用户意见反馈\[`不重要`]



## 栏目设置

#### 个人空间(所有人):

查看个人信息
修改个人信息
修改个人账号密码

### 找资源(所有人):(寻找课件)

搜索: (文件名,文件类型,提交时间范围,`点击量`)
展示: 图片封面,文件名

### 课件管理(教师,学校管理,管理员):

如果是教师登陆,课件信息仅为本人的课件

新增课件,删除课件,修改课件信息,查看课件信息

### 教师管理(学校管理,管理员)

增删改查 教师

### 学校管理(学校管理,管理员)

学校信息更新(学校管理,学校管理员)

学校新增,删除(管理员)



![页面](https://gitee.com/dashi_tata/images/raw/master/img/20200919222856.png)