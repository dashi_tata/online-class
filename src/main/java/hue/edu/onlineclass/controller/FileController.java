package hue.edu.onlineclass.controller;

import hue.edu.onlineclass.core.Result;
import hue.edu.onlineclass.core.ResultGenerator;
import hue.edu.onlineclass.service.FileHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author dashi
 */
@Controller
public class FileController {

    @Autowired
    private FileHandlerService fileHandlerService;

    /**
     * 图片上传
     */
    @RequestMapping("load-file")
    @ResponseBody
    public Result uploadPic(@RequestParam("file") MultipartFile file,@RequestParam("courseId") String courseId,@RequestParam("oprType") Integer oprType){
        String uuid=fileHandlerService.uploadPicFile(file,courseId,oprType);
        Integer effectLine=0;
        if(uuid==null){
            return ResultGenerator.genFailResult("添加图片失败");
        }else{
            return ResultGenerator.genSuccessMsgResult(uuid);
        }
    }

    /**
     * 图片映射
     */

    /**
     * 附件上传
     */
    @RequestMapping("/uploadFile")
    @ResponseBody
    public Object uploadFile(@RequestParam("file") MultipartFile file,
                             @RequestParam("courseId") String courseId,
                             @RequestParam("fileName") String fileName) {
        return fileHandlerService.uploadFile(file,courseId);
    }
    /**
     * 附件映射
     */
    /**
     * 视频上传
     */
    @RequestMapping("/uploadVideo")
    @ResponseBody
    public Result uploadVideo(@RequestParam("file") MultipartFile file,
                              @RequestParam("file1") MultipartFile file1,
                              @RequestParam("courseId")String courseId,
                              @RequestParam("fileName") String fileName) {
        String uuid=fileHandlerService.uploadVideo(file,file1,courseId,fileName);
        Integer effectLine=0;
        if(uuid==null){
            return ResultGenerator.genFailResult("添加录播失败");
        }else{
            return ResultGenerator.genSuccessMsgResult(uuid);
        }
    }
    /**
     * 视频下载
     */
}
