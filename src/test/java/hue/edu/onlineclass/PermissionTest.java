package hue.edu.onlineclass;

import hue.edu.onlineclass.enums.PermissionEnum;
import hue.edu.onlineclass.model.PermissionUrl;
import hue.edu.onlineclass.model.School;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.service.PermissionUrlService;
import hue.edu.onlineclass.service.SchoolService;
import hue.edu.onlineclass.service.UserService;
import hue.edu.onlineclass.utils.PassWordUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;

@SpringBootTest
public class PermissionTest {



    @Autowired
    private SchoolService schoolService;

    @Test
    public void addSchool() {
        String[] str = {"浙江大学", "四川大学", "吉林大学", "山东大学", "湖南大学",
                "云南大学", "辽宁大学", "黑龙江大学","江苏大学", "湖北大学", "安徽大学",
                "新疆大学","宁夏大学" ,"广西大学" ,"西藏大学"};
        for (String s : str) {
            School school = new School();
            school.setName(s);
            school.setAddress(s.replace("大学", ""));
            schoolService.addSchool(school);
        }
    }

    @Autowired
    private UserService userService;

    @Test
    public void addUser() {
        User user = new User();
        user.setStatus(true);
        user.setUsername("admin");
        user.setPassword("111111");
        user.setPermissionUuid(PermissionEnum.admin.name());
        userService.addUser(null,user);
    }

    @Test
    void testMd5(){

        System.out.println(PassWordUtils.getMd5("111111"));
    }


}
