package hue.edu.onlineclass.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件索引表格
 *
 * @author dashi
 */
@Data
@NoArgsConstructor
@TableName("tb_file")
public class FileHandler {
    @TableId
    private String id;
    /**
     * 网络映射句柄
     */
    @TableField("handler")
    private String handler;
    /**
     * 本地映射地址
     */
    @TableField("localpath")
    private String fileName;

    public FileHandler(String id, String handler, String fileName) {
        this.id = id;
        this.handler = handler;
        this.fileName = fileName;
    }
}
