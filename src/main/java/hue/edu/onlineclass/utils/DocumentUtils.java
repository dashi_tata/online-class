package hue.edu.onlineclass.utils;

import cn.hutool.core.io.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Slf4j
public class DocumentUtils {
    /**
     * 复制文件
     */
    public static Boolean saveFile(MultipartFile file, String localFileName){
        File localFile = new File(localFileName);
        localFile.getParentFile().mkdirs();
        try {
            file.transferTo(localFile);
        } catch (IOException e) {
            log.error("文件复制出错");
            return false;
        }
        log.info("本地文件:"+localFileName+"保存成功");
        return true;
    }

    /**
     * 获得文件后缀(类型
     */
    public static String getType(String fileName) {
        return FileUtil.extName(fileName);
    }

    /**
     * 获得文件名
     */
    public static String getFileName(String fileName) {
        return FileUtil.mainName(fileName);
    }
}
