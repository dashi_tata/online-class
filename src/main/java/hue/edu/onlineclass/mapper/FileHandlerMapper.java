package hue.edu.onlineclass.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.model.CourseFile;
import hue.edu.onlineclass.model.FileHandler;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dashi
 */
@Mapper
public interface FileHandlerMapper extends BaseMapper<FileHandler> {
}
