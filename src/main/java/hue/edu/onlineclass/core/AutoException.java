package hue.edu.onlineclass.core;

public class AutoException extends Exception {
    public AutoException(String message) {
        super(message);
    }
}
