package hue.edu.onlineclass.enums;

/**
 *
 * @author dashi
 */
public enum PermissionEnum {

    /**
     * 管理员
     */
    admin(1,"管理员"),
    /**
     * 教师
     */
    teacher(2,"教师"),
    /**
     * 学生
     */
    student(3,"学生"),
    /**
     * 学校管理员
     */
    schoolAdmin(4,"学校管理员");


    /**
     * 描述
     */
    public Integer id;
    public String desc;

    private PermissionEnum(Integer id, String desc){
        this.id = id;
        this.desc = desc;
    }
}
