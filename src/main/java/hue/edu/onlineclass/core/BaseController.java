package hue.edu.onlineclass.core;

import lombok.extern.slf4j.Slf4j;
//import org.lf.admin.api.baseapi.service.base.AccountServ
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.User;

import javax.servlet.http.HttpServletRequest;

/**
 * @author dashi
 */
@Slf4j
public abstract class BaseController {

//    @Autowired
//    protected AccountService accountService;

    @Autowired
    private HttpServletRequest request;

    protected String redirect(String url) {
        return new StringBuilder("redirect:").append(url).toString();
    }

//    protected LoginUser loginUser() {
//        SecurityContext context = SecurityContextHolder.getContext();
//        Object principal = context.getAuthentication().getPrincipal();
//        String username = ((User) principal).getUsername();
//        if (principal instanceof User)
//            return accountService.findLoginUser(username);
//        throw new ServiceException("用户未登录");
//    }
//
//    protected <T extends FilterVM> T wrapperFilterVM(T filterVM) {
//        String scope = (String) request.getAttribute("Level");
//        filterVM.setScope(scope);
//        return filterVM;
//    }
}
