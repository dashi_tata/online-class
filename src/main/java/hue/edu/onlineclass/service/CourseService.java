package hue.edu.onlineclass.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.enums.PermissionEnum;
import hue.edu.onlineclass.mapper.ChooseCourseMapper;
import hue.edu.onlineclass.mapper.CourseMapper;
import hue.edu.onlineclass.mapper.CourseVideoMapper;
import hue.edu.onlineclass.mapper.UserMapper;
import hue.edu.onlineclass.model.ChooseCourse;
import hue.edu.onlineclass.model.Course;
import hue.edu.onlineclass.model.CourseVideo;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.vo.CourseSearch;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author gcc
 */
@Service
public class CourseService {
    /**
     * 课程管理:
     *  课程列表查看
     *  课程新增
     *  课程删除
     *  课程详情
     */

    @Resource
    private CourseMapper courseMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private ChooseCourseMapper chooseCourseMapper;
    @Resource
    private CourseVideoMapper courseVideoMapper;


    /**
     * 查看课程列表
     */
    public Page<Course> getList(CourseSearch search, Page<Course> page){
        // 模糊搜索
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        if (Strings.isNotBlank(search.getName())){
            wrapper.like("name",search.getName());
        }
        if (Strings.isNotBlank(search.getTeacherId())){
            wrapper.eq("teacher_id",search.getTeacherId());
        }
        if (Strings.isNotBlank(search.getType())){
            wrapper.like("type",search.getType());
        }
        return courseMapper.selectPage(page, wrapper);
    }

    /**
     * 查看 课程详情
     */
    public Course getCourseInfo(String id){
        return courseMapper.selectById(id);
    }

    /**
     * 新增课程
     */
    public Integer addCourse(Course course){
        return courseMapper.insert(course);
    }

    /**
     * 更新课程
     */
    public Integer updateCourse(Course course){
        return courseMapper.updateById(course);
    }

    /**
     * 删除课程
     */
    public Integer delCourse(String id){
        return courseMapper.deleteById(id);
    }
    /**
     * 获取课程id对应的所有courseVideo集合
     */
    public List<CourseVideo> getVideoList(String courseId){
        return courseVideoMapper.getVideoList(courseId);
    }

    /**
     * 获得选该课的所有学生
     */
    public List<User> getChooseStudents(String courseId){
        List<String> sIds = getChooseStudentIds(courseId);
        return userMapper.selectBatchIds(sIds);
    }
    /**
     * 获得未选该课的所有学生
     */
    public List<User> getNotChooseStudentIds(String courseId){
        List<String> sIds = getChooseStudentIds(courseId);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("permission_uuid", PermissionEnum.student.name());
        queryWrapper.notIn("id",sIds);
        return userMapper.selectList(queryWrapper);
    }

    /**
     * 获得选该课的所有学生id
     */
    private List<String> getChooseStudentIds(String courseId){
        QueryWrapper<ChooseCourse> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id", courseId);
        List<ChooseCourse> chooseCourses = chooseCourseMapper.selectList(queryWrapper);
        List<String> sIds = new ArrayList<>();
        chooseCourses.forEach(chooseCourse -> sIds.add(chooseCourse.getStudentId()));
        return sIds;
    }


    /**
     * 查看课程列表 - 分页
     */
    /*public Page<Course> getPage(CourseSearch search, Page<Course> page){
        // 模糊搜索
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        if (Strings.isNotBlank(search.getName())){
            wrapper.like("name",search.getName());
        }
        if (Strings.isNotBlank(search.getTeacherId())){
            wrapper.eq("teacher_id",search.getTeacherId());
        }
        if (Strings.isNotBlank(search.getType())){
            wrapper.like("type",search.getType());
        }
        return courseMapper.selectPage(page, wrapper);
    }*/
}
