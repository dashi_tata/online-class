package hue.edu.onlineclass.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.core.Result;
import hue.edu.onlineclass.core.ResultGenerator;
import hue.edu.onlineclass.enums.CourseTypeEnum;
import hue.edu.onlineclass.enums.PermissionEnum;
import hue.edu.onlineclass.model.Course;
import hue.edu.onlineclass.model.CourseVideo;
import hue.edu.onlineclass.model.School;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.service.*;
import hue.edu.onlineclass.utils.PageUtils;
import hue.edu.onlineclass.utils.ResultUtils;
import hue.edu.onlineclass.vo.CourseSearch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author gcc&cmm
 */
@Slf4j
@Controller
@RequestMapping("course")
public class CourseController {
    /**
     * 课程管理:
     * 课程列表查看
     * 课程新增
     * 课程删除
     * 课程详情
     */
    @Autowired
    private CourseService courseService;
    @Autowired
    private UserService userService;
    @Autowired
    private FileHandlerService fileHandlerService;
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private ChooseCourseService chooseCourseService;


    /**
     * 课程列表-分页
     * 如果是老师的话就直接返回老师的数据
     */

    @RequestMapping("list")
    public String getCoursePage(Model model, @ModelAttribute("search") CourseSearch search,
                                Integer pageNum,HttpSession session) {
        User user = (User)session.getAttribute("user");
        if (PermissionEnum.teacher.name().equals(user.getPermissionUuid())){
            // 如果是老师的话，返回自己的
            search.setTeacherId(user.getId());
        }
        Page defaultPage = PageUtils.defaultPage(pageNum);
        Page page = courseService.getList(search, defaultPage);
        List<School> schoolList = schoolService.getSchoolList();
        model.addAttribute("page", page);
        model.addAttribute("schoolList", schoolList);
        model.addAttribute("types", CourseTypeEnum.values());
        model.addAttribute("typesMap", CourseTypeEnum.getCourseMap());
        return "course/list";
    }


    /**
     * 获得课程详情
     */
    @RequestMapping("info")
    public String geCourseInfo(Model model, String id) {
        // 课程信息  教师信息，课程类型
        Course courseInfo = courseService.getCourseInfo(id);
        String type = CourseTypeEnum.valueOf(courseInfo.getType()).desc;
        String teacherName = userService.getUserInfo(courseInfo.getTeacherId()).getName();
        model.addAttribute("course", courseInfo);
        model.addAttribute("type", type);
        model.addAttribute("teacherName", teacherName);

        // 视频列表
        List<CourseVideo> courseVideos = courseService.getVideoList(id);
        model.addAttribute("courseVideos", courseVideos);

        // 已选学生列表
        List<User> chooseStudents = courseService.getChooseStudents(id);
        model.addAttribute("chooseStudents", chooseStudents);

        // 未选学生列表
        List<User> notChooseStudents = courseService.getNotChooseStudentIds(id);
        model.addAttribute("students", notChooseStudents);

        model.addAttribute("id", id);
        return "course/info";
    }

    /**
     * 新增 课程 页面
     */
    @RequestMapping("add-page")
    public String openAddCoursePage(Model model) {
        model.addAttribute("types", CourseTypeEnum.values());
        return "course/add";
    }


    /**
     * 新增 课程
     */
    @ResponseBody
    @RequestMapping("add")
    public Result addCourse(Course course) {
        log.info(course.toString());
        Integer effectLine = courseService.addCourse(course);
        return ResultUtils.genResultByEffectLine(effectLine, "成功新增课程", "新增课程失败,请联系管理员");
    }

    /**
     * 修改课程页面
     */
    @RequestMapping("edit-page")
    public String openEditCoursePage(Course course) {
        return "course/edit";
    }

    /**
     * 修改课程
     */
    @ResponseBody
    @RequestMapping("edit")
    public Result editCourse(Course course) {
        Integer effectLine = courseService.updateCourse(course);
        return ResultUtils.genResultByEffectLine(effectLine, "成功修改课程", "修改课程失败,请联系管理员");
    }

    /**
     * 删除课程
     */
    @ResponseBody
    @RequestMapping("delete")
    public Result deleteCourse(String id) {
        return ResultUtils.genResultByEffectLine(courseService.delCourse(id),
                "成功删除课程", "删除失败,请联系管理员");
    }
    /**
     * 删除课程-视频附件
     */
    @ResponseBody
    @RequestMapping("delete-video")
    public Result deleteCourseVedio(String id) {
        return ResultUtils.genResultByEffectLine(fileHandlerService.delVideo(id),
                "成功删除课程", "删除失败,请联系管理员");
    }


    /**
     * 老师登陆的时候
     * 从课程页面进行制定学生
     */
    @ResponseBody
    @RequestMapping("add-students")
    public Result addStudents(HttpSession session, String[] ids, String courseId) {
        log.info(ids.toString());
        User user = (User) session.getAttribute("user");
        List<String> errorIds = chooseCourseService.setChooseCourseByStudentId(ids, user.getId(), courseId);
        if (errorIds.isEmpty()) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("id中可能出错").setData(errorIds);
        }
    }


    /**
     * 课程列表-分页
     */
    /*@RequestMapping("list-teach")
    public String getCoursePageSelf(Model model, HttpSession session, @ModelAttribute("search") CourseSearch search, Integer pageNum) {
        User user = (User) session.getAttribute("user");
        Page defaultPage = PageUtils.defaultPage(pageNum);
        Page page = courseService.getPage(search, defaultPage);
        search.setTeacherId(user.getId());

        List<School> schoolList = schoolService.getSchoolList();
        model.addAttribute("page", page);
        model.addAttribute("schoolList", schoolList);
        return "course/list";
    }*/

    /**
     * 课程list
     */
    /*@RequestMapping("/list-test")
    public String courseList(String id) {
        return "course/coursefile-list";
    }*/


}
