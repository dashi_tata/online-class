package hue.edu.onlineclass.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.DigestUtils;

/**
 * 密码工具类
 * @author dashi
 */
public class PassWordUtils {

    private static final String slat="class";
    /**
     * 字符串 生成md5 对应的密码字符串
     */
    public static String getMd5(String str) {
        String base = str +"/"+slat;
        // String md5 = DigestUtils.md5DigestAsHex(base.getBytes());
        return DigestUtils.md5DigestAsHex(base.getBytes());
    }

}
