package hue.edu.onlineclass.controller;

import hue.edu.onlineclass.core.Result;
import hue.edu.onlineclass.core.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author dashi
 */
//@Controller
//@RequestMapping("video")
public class VideoController {
    /**
     * 视频文件的上传下载,列表搜索
     */

    /**
     * 视频上传
     */

    @RequestMapping("upload")
    public Result videoUpload(MultipartFile file){
        if(file.isEmpty()){
            return ResultGenerator.genFailResult("错误的文件");
        }
        return null;
    }

    /**
     * 视频下载
     */
    @RequestMapping("download")
    public Result videoDownLoad(Integer id){
        if (id==null){
            return ResultGenerator.genFailResult("错误的视频id");
        }
        return null;
    }

    /**
     * 通过获得视频列表
     */
    public Result videoList(String courseId){
        return null;
    }



}
