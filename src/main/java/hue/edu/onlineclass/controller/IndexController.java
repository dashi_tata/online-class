package hue.edu.onlineclass.controller;

import hue.edu.onlineclass.core.BaseController;
import hue.edu.onlineclass.core.Result;
import hue.edu.onlineclass.core.ResultGenerator;
import hue.edu.onlineclass.enums.PermissionEnum;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @author dashi
 */

@Controller
public class IndexController extends BaseController {

    @Autowired
    private UserService userService;

    /**
     * 首页
     */
    @RequestMapping({"/", "/login"})
    public String loginPage() {
        return "login";
    }
/**
     * 首页
     */
    @RequestMapping({"/account-error"})
    public String accountError() {
        return "account-error";
    }

    /**
     * 验证登陆
     */
    @RequestMapping("/check-login")
    public String checkLogin(String username, String password, HttpSession session, Model model) {
        User user = userService.checkLogin(username, password);
        if (user != null && !(PermissionEnum.student.name().equals(user.getPermissionUuid()))) {
            session.setAttribute("user", user);
            if (PermissionEnum.admin.name().equals(user.getPermissionUuid())) {
                //管理员
                return redirect("/user/list");
            }
            if (PermissionEnum.teacher.name().equals(user.getPermissionUuid())) {
                return redirect("/course/list");
            }
        }

        model.addAttribute("MSG", "用户名,密码错误");
        return "login";
    }




    /**
     * 检查用户名是否重复
     */
    @RequestMapping("/check-username-repeat")
    @ResponseBody
    public Result checkUserNameRepeat(String username) {
        Boolean isRepeatUserName = userService.hasRepeatUserName(username);
        if (isRepeatUserName) {
            return ResultGenerator.genFailResult("用户名重复");
        } else {
            return ResultGenerator.genSuccessResult();
        }
    }

    /**
     * 退出用户
     */
    @RequestMapping("/singout")
    public String singout(HttpSession session) {
        session.removeAttribute("user");
        return redirect("/");
    }

    /**
     * 注册页面 弃用
     */
    /*@RequestMapping("/register-page")
    public String getRegisterPage() {
        return "register";
    }*/

    /**
     * 注册 弃用
     * @param username
     * @param password
     * @return
     */
    /*@RequestMapping("/register")
    @ResponseBody
    public Result register(String username, String password) {
        User user = new User(username, password, PermissionEnum.student.name());
        user.setStatus(true);
        if (userService.hasRepeatUserName(username)) {
            return ResultGenerator.genFailResult("用户名重复,再想一个吧!");
        }
        Integer effectLine = userService.addUser(null, user);
        return ResultUtils.genResultByEffectLine(effectLine, "成功创建,快去登陆吧", "创建失败,请联系管理员");
    }*/
}
