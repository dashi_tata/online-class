package hue.edu.onlineclass.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 课程表
 * @author liangzhu
 */
@Data
@TableName("tb_course")
public class Course {
    /**
     * 课程id
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 课程名
     */
    @TableField("name")
    private String name;
    /**
     * 课程类型, 来自TypeEnum
     */
    @TableField("type")
    private String type;
    /**
     * 教师id
     */
    @TableField("teacher_id")
    private String teacherId;
    /**
     * 课程信息
     */
    @TableField("info")
    private String info;
    /**
     * 图片信息
     */
    @TableField("pic_path")
    private String picPath;
    /**
     * 点击量
     */
    @TableField("count")
    private String count;

}
