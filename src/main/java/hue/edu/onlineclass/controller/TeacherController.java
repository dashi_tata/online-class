package hue.edu.onlineclass.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.service.UserService;
import hue.edu.onlineclass.vo.UserSearch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author dashi
 */
@Slf4j
//@Controller
//@RequestMapping("teacher")
public class TeacherController {

    /**
     * 教师管理
     * 教师的增删改查
     */

    @Autowired
    private UserService userService;

    /**
     * 查看教师列表
     */
    public Page<User> getTeacherPage(@ModelAttribute("search") UserSearch search, Model model){
        return null;
    }


}
