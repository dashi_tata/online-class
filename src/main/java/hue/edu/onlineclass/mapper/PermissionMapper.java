package hue.edu.onlineclass.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.model.Permission;
import org.apache.ibatis.annotations.Mapper;

/***
 * @author 梁柱
 */

@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

}
