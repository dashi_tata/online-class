package hue.edu.onlineclass.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.model.CourseFile;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dashi
 */
@Mapper
public interface CourseFileMapper extends BaseMapper<CourseFile> {
    @Insert("insert into tb_course_file(name,course_id,file_handler) values(#{name},#{courseId},#{fileHandler})")
    public int insertFile(CourseFile courseFile);
}
