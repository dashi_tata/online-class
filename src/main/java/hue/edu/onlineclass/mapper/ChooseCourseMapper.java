package hue.edu.onlineclass.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.model.ChooseCourse;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author dashi
 */
@Mapper
public interface ChooseCourseMapper extends BaseMapper<ChooseCourse> {
}
