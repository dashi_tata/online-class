package hue.edu.onlineclass.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 用于关键字搜索课程
 * @author dashi
 */
@Data
public class CourseSearch {
    /**
     * 课程名
     */
    private String name;
    /**
     * 课程类型, 来自TypeEnum
     */
    private String type;
    /**
     * 课程信息
     */
    private String info;
    /**
     * 教师id
     */
    private String teacherId;
    /**
     * 图片信息
     */
    private String picPath;
}
