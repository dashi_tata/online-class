package hue.edu.onlineclass.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.model.Course;
import hue.edu.onlineclass.model.CourseVideo;
import hue.edu.onlineclass.model.School;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author gcc&cmm
 */
@Mapper
public interface  CourseMapper extends BaseMapper<Course> {
    @Insert("insert into tb_course(id,name,type,teacher_id,info,pic_path) values(#{id},#{name},#{type},#{teacherId},#{info},#{picPath})")
    public int insertCourse(Course course);
}
