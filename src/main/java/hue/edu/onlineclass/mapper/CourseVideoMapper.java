package hue.edu.onlineclass.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.model.CourseVideo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CourseVideoMapper extends BaseMapper<CourseVideo> {
    @Insert("insert into tb_course_file(name,course_id,file_handler) values(#{name},#{courseId},#{fileHandler})")
    public int insertVideo(CourseVideo courseVideo);
    @Select("select id,name,file_handler,pic_path from tb_course_video where course_id=#{courseId}")
    public List<CourseVideo> getVideoList(String courseId);
}
