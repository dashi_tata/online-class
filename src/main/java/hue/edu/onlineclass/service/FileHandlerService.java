package hue.edu.onlineclass.service;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.mapper.CourseFileMapper;
import hue.edu.onlineclass.mapper.CourseMapper;
import hue.edu.onlineclass.mapper.CourseVideoMapper;
import hue.edu.onlineclass.mapper.FileHandlerMapper;
import hue.edu.onlineclass.model.Course;
import hue.edu.onlineclass.model.CourseFile;
import hue.edu.onlineclass.model.CourseVideo;
import hue.edu.onlineclass.model.FileHandler;
import hue.edu.onlineclass.utils.DocumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dashi
 */

@Slf4j
@Service
public class FileHandlerService {


    @Value("${document.path.video.handler}")
    private String courseVideoHandler;
    @Value("${document.path.video.localPath}")
    private String courseVideoLocalPath;
    @Value("${document.path.file.handler}")
    private String courseFileHandler;
    @Value("${document.path.file.localPath}")
    private String courseFileLocalPath;

    @Value("${document.path.pic.handler}")
    private String picFileHandler;
    @Value("${document.path.pic.localPath}")
    private String picFileLocalPath;

    @Resource
    private FileHandlerMapper fileHandlerMapper;
    @Resource
    private CourseFileMapper courseFileMapper;
    @Resource
    private CourseVideoMapper courseVideoMapper;
    @Resource
    private CourseMapper courseMapper;
    /**
     * 图片部分
     */

    /**
     * 通过id获得图片句柄
     */
    public FileHandler getPicFile(String uuid) {
        return fileHandlerMapper.selectById(uuid);
    }

    /**
     * 图片上传
     */
    public String uploadPicFile(MultipartFile file,String courseId,Integer oprType) {
        //调用保存文件方法
        List<String> list = createFile(file, picFileHandler, picFileLocalPath);
        //保存成功就执行插入方法
        if (list.size() != 0) {
            //返回受影响的行数
            Integer effectLine = insertTable(list, fileHandlerMapper, 1);
            if (effectLine.equals(0)) {//effectLine==0说明插入数据失败
                //删除已经保存的文件
                File file1 = new File(picFileLocalPath + list.get(1));
                if (file1.exists()) {
                    file1.delete();
                }
                return null;
            } else {//插入成功就返回图片的webPath地址
                Integer in=new Integer(1);
                if(in.equals(oprType)){
                    Course course=new Course();
                    course.setId(courseId);
                    course.setPicPath(list.get(3));
                    Integer effect=courseMapper.updateById(course);
                    if(effect==0){
                        return null;
                    }else{
                        return list.get(3);
                    }
                }
                return list.get(3);
            }
        } else {
            return null;
        }
//        return upload(file,null,picFileHandler,picFileLocalPath,fileHandlerMapper,1,null,null);
    }

    /**
     * 删除图片
     */
    public Integer delPicFile(String uuid) {
        return fileHandlerMapper.deleteById(uuid);
    }


    /**
     * 附件部分
     */
    public String uploadFile(MultipartFile file, String courseId) {
        List<String> list = createFile(file, courseFileHandler, courseFileLocalPath);
        if (list.size() != 0) {
            //list1是要插入表格数据
            List<String> list1 = new ArrayList<>();
            list1.add(list.get(1));
            list1.add(courseId);
            list1.add(list.get(0));
            Integer effectLine = insertTable(list, courseFileMapper, 2);
            if (effectLine.equals(0)) {
                File file1 = new File(courseFileLocalPath + list.get(1));
                if (file1.exists()) {
                    file1.delete();
                }
                return null;
            } else {//插入成功返回uuid
                return list.get(0);
            }
        } else {
            return null;
        }
//        return upload(file,null,courseFileHandler,courseFileLocalPath,courseFileMapper,2,courseId,null);
    }
    /**
     * 通过id获得图片句柄
     */

    /**
     * 视频部分
     */
    public String uploadVideo(MultipartFile file, MultipartFile file1, String courseId, String fileName) {
        //先判断视频封面文件是否创建成功
        String webPath = uploadPicFile(file1,null,null);
        if (webPath != null) {
            List<String> list = createFile(file, courseVideoHandler, courseVideoLocalPath);
            if (list.size() != 0) {
                String uuid = list.get(0);
                String localFileName = list.get(1);
                String webVideoPath = list.get(3);
                list.clear();
                //换成要插入表格的数据，也可以自己在创建一个List对象
                list.add(fileName);
                list.add(courseId);
                // list.add(uuid);
                list.add(webVideoPath);
                list.add(webPath);
                Integer effectLine = insertTable(list, courseVideoMapper, 3);
                if (effectLine != 0) {
                    //插入成功返回uuid
                    return list.get(0);
                } else {
                    File file2 = new File(courseVideoLocalPath + localFileName);
                    if (file2.exists()) {
                        file2.delete();
                    }
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
//        return upload(file,file1,courseVideoHandler, courseVideoLocalPath, courseVideoMapper, 3,courseId,fileName);
    }

//    /**
//     * 获取文件列表
//     */
//    public Page<CourseFile> fileDownLoad(String courseId, Page<CourseFile> page) {
//        QueryWrapper<CourseFile> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("course_num", courseId);
//        return courseFileMapper.selectPage(page, queryWrapper);
//    }
//
//    /**
//     * 删除文件
//     */
//    public Integer deleteCourseFile(Integer id) {
//        return courseFileMapper.deleteById(id);
//    }
//
//    /**
//     * 更新文件名
//     */
//    public Integer updateCourseFile(Integer id, String fileName) {
//        CourseFile courseFile = new CourseFile();
//        courseFile.setId(id);
//        courseFile.setName(fileName);
//        return courseFileMapper.updateById(courseFile);
//    }

    /**
     * 创建文件夹保存文件
     *
     * @param file       要保存的文件
     * @param webHandler
     * @param localPath
     * @return
     */
    public List<String> createFile(MultipartFile file, String webHandler, String localPath) {
        List<String> list = new ArrayList<>();
        // 获得文件名
        String originalFileName = file.getOriginalFilename();
        String fileName = DocumentUtils.getFileName(originalFileName);
        log.info("fileName" + fileName);
        // 获得文件类型
        String filetype = DocumentUtils.getType(originalFileName);
        log.info("fieltype" + filetype);
        // 创建新的文件名
        String uuid = IdUtil.simpleUUID();

        // 全文件名设定
        String localFileName = uuid;
        if (Strings.isNotBlank(filetype)) {
            localFileName += "." + filetype;
        }
        // 复制文件到本地
        if (DocumentUtils.saveFile(file, localPath + localFileName)) {
            // 获得网络文件链接
            String webPath = webHandler.replace("**", localFileName);
            //向list中添加需要返回的数据
            list.add(uuid);
            list.add(localFileName);
            list.add(fileName);
            list.add(webPath);
            return list;
        } else {
            return list;
        }
    }

    /**
     * 向表格中插入数据
     *
     * @param list
     * @param baseMapper
     * @param oprType    通过这个字段判断插入什么类型类型的数据
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Integer insertTable(List<String> list, BaseMapper baseMapper, Integer oprType) {
        int effectLine = 0;
        Object object = null;
        //插入的是fileHandler对象
        if (oprType.equals(1)) {
            object = new FileHandler(list.get(0), list.get(1), list.get(2));
        } else if (oprType.equals(2)) {
            //插入的是courseFile对象
            object = new CourseFile(list.get(0), list.get(1), list.get(2));
        } else {
            //插入的是courseVideo对象
            object = new CourseVideo(list.get(0), list.get(1), list.get(2), list.get(3));
        }
        log.info(object.toString());
        try {
            effectLine = baseMapper.insert(object);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            effectLine = 0;
        }
        return effectLine;
    }
//    public String upload(BaseMapper baseMapper,Integer oprType,String courseId,String fName) {
//        // 获得文件名
//        String originalFileName = file.getOriginalFilename();
//        String fileName = DocumentUtils.getFileName(originalFileName);
//        log.info("fileName"+fileName);
//        // 获得文件类型
//        String filetype = DocumentUtils.getType(originalFileName);
//        log.info("fieltype"+filetype);
//        // 创建新的文件名
//        String uuid = IdUtil.simpleUUID();
//        // 全文件名设定
//        String localFileName = uuid;
//        if (Strings.isNotBlank(filetype)) {
//            localFileName += "." + filetype;
//        }
//        // 复制文件到本地
//        DocumentUtils.saveFile(file, localPath + localFileName);
//        // 获得网络文件链接
//        String webPath = webHandler.replace("**", localFileName);
//        // 保存到数据库
//        int effectLine=0;
//        Object object=null;
//        if(oprType.equals(1)){
//            object= new FileHandler(uuid, localFileName, fileName);
//            log.info(object.toString());
//            effectLine = baseMapper.insert(object);
//        }else if(oprType.equals(2)){
//            object=new CourseFile(localFileName,courseId,uuid);
//            log.info(object.toString());
//            effectLine = baseMapper.insert(object);
//        }else if(oprType.equals(3)){
//            String imgWebPath = null;
//            if (file1!=null){
//                String localName = uuid;
//                String originalFile1Name = file1.getOriginalFilename();
//                String ftype = DocumentUtils.getType(originalFile1Name);
//                if (Strings.isNotBlank(ftype)) {
//                    localName += "." + ftype;
//                }
//                // 复制文件到本地
//                File file2=new File(picFileLocalPath + localName);
//                try {
//                    file1.transferTo(file2);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
////                DocumentUtils.saveFile(file, picFileLocalPath + localName);
//                // 获得网络文件链接
//                imgWebPath = picFileHandler.replace("**", localName);
//                object=new CourseVideo(fName,courseId,uuid,imgWebPath);
//            }else{
//                object=new CourseVideo(fName,courseId,uuid,null);
//            }
//            log.info(object.toString());
//             effectLine = baseMapper.insert(object);
//        }
//        //插入错误
//        if (effectLine == 0) {
//            return null;
//        }
//        return uuid;
//    }

    /**
     * 通过id删除video
     */
    public Integer delVideo(String id){
        return courseVideoMapper.deleteById(id);
    }
}
