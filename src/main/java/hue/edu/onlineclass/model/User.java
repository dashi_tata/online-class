package hue.edu.onlineclass.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@TableName("tb_user")
public class User {

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 用户名
     */
    @TableField("username")
    private String username;
    /**
     * 密码
     */
    @TableField("password")
    private String password;
    /**
     * 姓名
     */
    @TableField("name")
    private String name;
    /**
     * 性别
     */
    @TableField("sex")
    private Boolean sex;
    /**
     * 通讯地址
     */
    @TableField("address")
    private String address;
    /**
     * 邮箱
     */
    @TableField("email")
    private String email;
    /**
     * 学校编码
     */
    @TableField("school_uuid")
    private String schoolId;
    /**
     * 权限
     */
    @TableField("permission_uuid")
    private String permissionUuid;
    /**
     * 介绍
     */
    @TableField("info")
    private String info;
    /**
     * 状态:是否被删除
     */
    @TableField("status")
    private Boolean status;
    /**
     * 头像管理
     */
    @TableField("pic_path")
    private String picPath;


    public User(String username, String password, String permissionUuid) {
        this.username = username;
        this.password = password;
        this.permissionUuid = permissionUuid;
    }
}
