package hue.edu.onlineclass.utils;

import hue.edu.onlineclass.core.Result;
import hue.edu.onlineclass.core.ResultGenerator;

public class ResultUtils {

    public static Result genResultByEffectLine(Integer effectLine,String successMessage,String failMessage){
        if (effectLine==0){
            return ResultGenerator.genFailResult(failMessage);
        }else {
            return ResultGenerator.genSuccessMsgResult(successMessage);
        }
    }

}
