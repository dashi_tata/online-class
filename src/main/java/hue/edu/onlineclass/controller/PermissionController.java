package hue.edu.onlineclass.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.core.Result;
import hue.edu.onlineclass.model.Permission;
import hue.edu.onlineclass.model.School;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.service.PermissionService;
import hue.edu.onlineclass.utils.PageUtils;
import hue.edu.onlineclass.utils.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author dashi
 * 该方法弃用
 */
//@Slf4j
//@Controller
//@RequestMapping("permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    /**
     * 新建权限(角色)
     *
     * 角色分配任务
     */


    @RequestMapping("list")
    public String getList(Model model, @ModelAttribute("search")Permission permission, Integer pageNum){
        Page page = PageUtils.defaultPage(pageNum);
        Page list = permissionService.getList(permission, page);
        model.addAttribute("list",list);
        return "permission/list";
    }

    @GetMapping("add")
    public String addPage(){
        return "permission/add";
    }

    @PostMapping("add")
    @ResponseBody
    public Result addPermission(Permission permission){
        Integer effectLine = permissionService.addOrEditPermission(permission);
        return ResultUtils.genResultByEffectLine(effectLine,"成功添加权限","添加权限失败,请联系管理员");
    }

    @GetMapping("edit")
    public String editPage(){
        return "permission/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    public Result editPermission(Permission permission){
        Integer effectLine = permissionService.addOrEditPermission(permission);
        return ResultUtils.genResultByEffectLine(effectLine,"成功修改权限","修改权限失败,请联系管理员");
    }

    @RequestMapping("info")
    public String getEditInfo(HttpSession session){
        Permission permission = (Permission) session.getAttribute("permission");
        return "permission/info";
    }

}