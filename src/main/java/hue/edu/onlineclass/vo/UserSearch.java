package hue.edu.onlineclass.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 用于User类型的搜索
 * @author dashi
 */
@Data
public class UserSearch {

    private String name;
    private Boolean sex;
    private String schoolId;
    private String permissionUuid;
    private Boolean status;


}
