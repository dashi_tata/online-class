package hue.edu.onlineclass.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 梁柱
 */
@Data
@NoArgsConstructor
@TableName("tb_course_file")
public class CourseFile {

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 文件名
     */
    @TableField("name")
    private String name;
    /**
     * 课程id
     */
    @TableField("course_id")
    private String courseId;
    /**
     * 文件地址
     */
    @TableField("file_handler")
    private String fileHandler;


    public CourseFile(String name,String courseId,String fileHandler){
        this.name = name;
        this.courseId = courseId;
        this.fileHandler = fileHandler;
    }
}
