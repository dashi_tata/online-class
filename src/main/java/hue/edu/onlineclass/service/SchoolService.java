package hue.edu.onlineclass.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.mapper.SchoolMapper;
import hue.edu.onlineclass.model.School;
import hue.edu.onlineclass.vo.SchoolSearch;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dashi
 */
@Service
public class SchoolService {

    /**
     * 学校管理:
     * 查看学校列表
     * 新增学校信息
     * 查看学校详细信息
     * 修改学校信息
     * 删除学校
     */
    @Resource
    private SchoolMapper schoolMapper;

    /**
     * 查看学校列表 分页
     */
    public Page<School> getPage(SchoolSearch search, Page<School> page){
        // 模糊搜索
        QueryWrapper<School> wrapper = new QueryWrapper<>();
        if (Strings.isNotBlank(search.getName())){
            wrapper.like("name","%"+search.getName()+"%");
        }
        if (Strings.isNotBlank(search.getAddress())){
            wrapper.like("address","%"+search.getAddress()+"%");
        }
        if (Strings.isNotBlank(search.getInfo())){
            wrapper.like("info","%"+search.getInfo()+"%");
        }
        return schoolMapper.selectPage(page, wrapper);
    }

    /**
     * 获得学校列表
     */
    public List<School> getSchoolList(){
        return schoolMapper.getSchoolList();
    }

    /**
     * 获得学校 map
     */
    public Map<String,String> getSchoolMap(){
        List<School> schoolList = schoolMapper.getSchoolList();
        Map<String,String> schoolMap = new HashMap<>();
        schoolList.forEach(school -> {
            schoolMap.put(school.getId(),school.getName());
        });
        return schoolMap;
    }

    /**
     * 新建 学校
     */
    public Integer addSchool(School school){
        return schoolMapper.insert(school);
    }
    /**
     * 修改 学校
     */
    public Integer editSchool(School school){
        return schoolMapper.updateById(school);
    }

    /**
     * 通过 id 查看 学校 信息
     */
    public School getSchoolInfo(String id){
        return schoolMapper.selectById(id);
    }

    /**
     * 删除学校
     */
    public Integer delSchool(Integer id){
        return schoolMapper.deleteById(id);
    }



}
