package hue.edu.onlineclass.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author dashi
 */
@Data
@TableName("tb_course_teacher_student")
public class ChooseCourse {
    @TableId(type = IdType.AUTO)
    private Integer id;
    @TableField("course_id")
    private String courseId;
    @TableField("teacher_id")
    private String teacherId;
    @TableField("student_id")
    private String studentId;
}
