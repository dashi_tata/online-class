package hue.edu.onlineclass.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author dashi
 */
@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Value("${document.path.video.handler}")
    private String courseVideoHandler;
    @Value("${document.path.video.localPath}")
    private String courseVideoPath;
    @Value("${document.path.file.handler}")
    private String courseFileHandler;
    @Value("${document.path.file.localPath}")
    private String courseFilePath;

    @Value("${document.path.pic.handler}")
    private String picFileHandler;
    @Value("${document.path.pic.localPath}")
    private String picFileLocalPath;
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("lose").setViewName("powerlose");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String[] excludePathPatterns = getExcludeCommonPathPatterns();

        //拦截器生效 --已经注释
//         registry.addInterceptor(permissionScopInterceptor).excludePathPatterns("/", "/login", "").excludePathPatterns(excludePathPatterns);
//         registry.addInterceptor("/asserts/**");"/asserts/**"
         registry.addInterceptor(new LoginHandlerInterceptor()).excludePathPatterns(excludePathPatterns);
    }

    // 映射 本地地址
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("新增附件本地句柄"+courseFileHandler+", 本地映射地址: "+courseFilePath);
        registry.addResourceHandler(courseFileHandler).addResourceLocations("file:"+courseFilePath);
        log.info("新增附件本地句柄"+courseVideoHandler+", 本地映射地址: "+courseVideoPath);
        registry.addResourceHandler(courseVideoHandler).addResourceLocations("file:"+courseVideoPath);
        log.info("新增附件本地句柄"+picFileHandler+", 本地映射地址: "+picFileLocalPath);
        registry.addResourceHandler(picFileHandler).addResourceLocations("file:"+picFileLocalPath);
    }
    private String[] getExcludeCommonPathPatterns() {
        String[] urls = {
                "/",
                "/error",
                "/account-error",
                "/login",
                "/lose",
                "/check-login",
                "/register",
                "/cus/register",
                "/cus/checkUserName",
                "/asserts/**",
                "/webjars/**",
                "/file/file/**"
        };
        return urls;
    }

}
