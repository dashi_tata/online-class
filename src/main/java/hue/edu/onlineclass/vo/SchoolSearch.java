package hue.edu.onlineclass.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author dashi
 */
@Data
public class SchoolSearch {
    /**
     * 学校名
     */
    private String name;
    /**
     * 学校地址
     */
    private String address;
    /**
     * 学校信息
     */
    private String info;
}
