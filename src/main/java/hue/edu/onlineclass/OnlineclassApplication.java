package hue.edu.onlineclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class OnlineclassApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineclassApplication.class, args);
    }

}
