package hue.edu.onlineclass.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.core.Result;
import hue.edu.onlineclass.model.PermissionUrl;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.service.PermissionUrlService;
import hue.edu.onlineclass.utils.PageUtils;
import hue.edu.onlineclass.utils.ResultUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("permissionUrl")
public class PermissionUrlController {
    /**
     * 权限Url管理
     * 权限Url的增删改查
     */
    @Autowired
    private PermissionUrlService permissionUrlService;

    @RequestMapping("list")
    public String getList(Model model, @ModelAttribute("search") PermissionUrl permissionUrl, Integer pageNum){
        Page page = PageUtils.defaultPage(pageNum);
        Page list = permissionUrlService.getinfo(permissionUrl, page);
        model.addAttribute("list", list);
        return "permissionUrl/list";
    }

    @GetMapping("add")
    public String addPage(){
        return "permissionUrl/add";
    }

    @PostMapping("add")
    @ResponseBody
    public Result addPermissionUrl(PermissionUrl permissionUrl){
        Integer effectLine = permissionUrlService.addPermissionUrl(permissionUrl);
        return ResultUtils.genResultByEffectLine(effectLine, "成功添加到权限", "添加权限失败");
    }

    @GetMapping("edit")
    public String editPage(){
        return "permissionUrl/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    public Result editPermissionUrl(PermissionUrl permissionUrl){
        Integer effectLine = permissionUrlService.addPermissionUrl(permissionUrl);
        return ResultUtils.genResultByEffectLine(effectLine, "成功修改权限", "修改权限失败");
    }

    @RequestMapping("info")
    public String getPermissionUrlInfo(HttpSession httpSession){
        User user =(User) httpSession.getAttribute("user");
        return "PermissionUrl/info";
    }

}
