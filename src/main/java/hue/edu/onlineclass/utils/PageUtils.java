package hue.edu.onlineclass.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author dashi
 */
public class PageUtils<T> {
    public static Page defaultPage(Integer pageNum){
        Page page = new Page();
        if (page.getSize()==0){
            page.setSize(10);
        }
        if (pageNum==null){
            pageNum=0;
        }
        page.setCurrent(pageNum);
        return page;
    }
}