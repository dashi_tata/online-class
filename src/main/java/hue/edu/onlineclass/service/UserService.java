package hue.edu.onlineclass.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.enums.PermissionEnum;
import hue.edu.onlineclass.mapper.UserMapper;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.utils.PassWordUtils;
import hue.edu.onlineclass.vo.UserSearch;
import org.apache.ibatis.annotations.Param;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author dashi
 */

@Service
public class UserService {

    @Resource
    private UserMapper userMapper;
    @Autowired
    private FileHandlerService fileHandlerService;

    /**
     * 功能:
     * 验证登陆
     * 获得用户信息
     */
    public User checkLogin(String username,String password) {
        return userMapper.checkLogin(username, PassWordUtils.getMd5(password));
    }

    /**
     * 用户的新增
     */
    public Integer addUser(MultipartFile file,User user) {
        if (file!=null){
            String webPath = fileHandlerService.uploadPicFile(file,null,null);
            user.setPicPath(webPath);
        }

        if (Strings.isNotBlank(user.getPassword())){
            user.setPassword(PassWordUtils.getMd5(user.getPassword()));
        }

        return userMapper.insert(user);
    }

    /**
     * 通过搜索到用户数据
     *
     * @param page         当前的页面
     * @return 返回的搜索数据
     */
    public Page<User> getUserPage(UserSearch search, Page<User> page) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (Strings.isNotBlank(search.getName())) {
            queryWrapper.like("name", search.getName());
        }
        if (Strings.isNotBlank(search.getSchoolId())) {
            queryWrapper.eq("school_uuid", search.getSchoolId());
        }
        if (search.getSex()!=null){
            queryWrapper.eq("sex",search.getSex());
        }
        if (Strings.isNotBlank(search.getPermissionUuid())) {
            queryWrapper.eq("permission_uuid", search.getPermissionUuid());
        }
        if (search.getStatus()!=null) {
            queryWrapper.eq("status", search.getStatus());
        }
        return userMapper.selectPage(page, queryWrapper);
    }



    /**
     * 用户的更新
     */
    public Integer updateUser(MultipartFile file, User user) {
        if (file!=null){
            String webPath = fileHandlerService.uploadPicFile(file,null,null);
            user.setPicPath(webPath);
        }
        if (Strings.isNotBlank(user.getPassword())){
            user.setPassword(PassWordUtils.getMd5(user.getPassword()));
        }
        return userMapper.updateById(user);
    }

    /**
     * 查看用户信息
     */
    public User getUserInfo(String id) {
        return userMapper.selectById(id);
    }

    /**
     * 删除用户
     */
    public Integer deleteUserById(Integer id) {
        return userMapper.deleteById(id);
    }

    /**
     * 用户名查重
     */
    public Boolean hasRepeatUserName(String username){
        Integer count = userMapper.checkRepeatUserName(username);
        return count != null && count > 0;
    }

    public List<User> getStudentList(){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("permission_uuid",PermissionEnum.student.name());
        return userMapper.selectList(wrapper);
    }


}
