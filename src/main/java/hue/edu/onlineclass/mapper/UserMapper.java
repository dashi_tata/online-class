package hue.edu.onlineclass.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author dashi
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 登陆查询
     * @param username 用户名
     * @param password 密码
     * @return 是否有该用户,用户信息
     */
    @Select("select * from tb_user where username like #{username} and password like #{password} and status = 1")
    User checkLogin(String username,String password);

    /**
     * 用户名查重
     * @param username 用户名
     * @return 该用户名数量
     */
    @Select("select count(username) from tb_user where username like #{username}")
    Integer checkRepeatUserName(String username);
    /**
     * 是否有该用户
     * @param id 用户id
     * @return 该用户名数量
     */
    @Select("select count(*) from tb_user where id like #{id}")
    Integer haveUserByUserId(String id);

}
