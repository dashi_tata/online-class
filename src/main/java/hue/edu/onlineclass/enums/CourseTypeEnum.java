package hue.edu.onlineclass.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dashi
 */
public enum CourseTypeEnum {
    /**
     * 类型
     */
    music("音乐"),
    sport("体育"),
    SkillHobby("技能爱好"),
    DanceFitness("舞蹈健身"),
    FlowerGardening("花卉园艺"),
    StylisticEntertainment("文体娱乐"),
    CulturalEducation("文化教育 ");


    public String desc;

    CourseTypeEnum(String desc) {
        this.desc = desc;
    }

    public static Map<String,String> getCourseMap(){
        CourseTypeEnum[] values = CourseTypeEnum.values();
        Map<String,String> map = new HashMap<>();
        for (CourseTypeEnum courseType : values) {
            map.put(courseType.name(), courseType.desc);
        }
        return map;
    }
}
