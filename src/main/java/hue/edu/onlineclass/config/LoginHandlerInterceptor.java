package hue.edu.onlineclass.config;

import hue.edu.onlineclass.enums.PermissionEnum;
import hue.edu.onlineclass.model.Permission;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Slf4j
@Primary
@Configuration
public class LoginHandlerInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info(request.getServletPath());
        HttpSession session = request.getSession();
        // 查询是否有该页面的权限(没写)
        User user = (User) session.getAttribute("user");

        if (user==null){
            request.getRequestDispatcher("/").forward(request,response);
            return false;
        }else if (PermissionEnum.teacher.name().equals(user.getPermissionUuid())){
            // 登陆身份是老师 屏蔽学生操作
            String servletPath = request.getServletPath();
            if (servletPath.startsWith("/user")){
                request.getRequestDispatcher("/account-error").forward(request,response);
                return false;
            }
        }
        return true;

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
