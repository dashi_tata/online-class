package hue.edu.onlineclass.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.core.Result;
import hue.edu.onlineclass.model.School;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.service.SchoolService;
import hue.edu.onlineclass.utils.PageUtils;
import hue.edu.onlineclass.utils.ResultUtils;
import hue.edu.onlineclass.vo.SchoolSearch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author dashi
 * 弃用
 */
@Slf4j
@Controller
@RequestMapping("school")
public class SchoolController {
    /**
     * 学校管理:
     *  学校的增删改查
     *
     */

    @Autowired
    private SchoolService schoolService;

    /**
     * 获得学校信息列表 分页
     */
    @RequestMapping("list")
    public String getPage(Model model, @ModelAttribute("search") SchoolSearch search, Integer pageNum){
        Page<School> page = PageUtils.defaultPage(pageNum);
        Page<School> schoolPage = schoolService.getPage(search, page);
        // log.info(schoolPage.getRecords().toString());
        List<School> schoolList = schoolService.getSchoolList();
        model.addAttribute("page",page);
        model.addAttribute("schoolList",schoolList);
        return "school/list";
    }
    /**
     * 获得学校信息列表 分页
     */
    @ResponseBody
    @RequestMapping("page-test")
    public Page<School> getPageTest(Model model, @ModelAttribute("search") SchoolSearch search, Integer pageNum){
        Page<School> page = PageUtils.defaultPage(pageNum);
        return schoolService.getPage(search, page);
    }/**
     * 获得学校信息列表 分页
     */
    @ResponseBody
    @RequestMapping("list-test")
    public List<School> getListTest(){
        return schoolService.getSchoolList();
    }

    /**
     * 添加学校页面
     */
    @RequestMapping("add-page")
    public String addPage(){
        return "school/add";
    }

    /**
     * 添加学校
     *
     */
    @RequestMapping("add")
    @ResponseBody
    public Result addSchool(School school){
        Integer effectLine = schoolService.addSchool(school);
        return ResultUtils.genResultByEffectLine(effectLine,"成功添加学校","添加学校失败,请联系管理员");
    }

    /**
     * 编辑学校页面
     */
    @RequestMapping("edit-page")
    public String editPage(String id,Model model){
        School schoolInfo = schoolService.getSchoolInfo(id);
        if (schoolInfo==null){
            return "eroor";
        }
        model.addAttribute("school",schoolInfo);
        return "school/edit";
    }

    /**
     * 编辑学校请求
     */
    @RequestMapping("edit")
    @ResponseBody
    public Result editSchool(School school){
        Integer effectLine = schoolService.editSchool(school);
        return ResultUtils.genResultByEffectLine(effectLine,"成功修改学校","修改学校失败,请联系管理员");
    }

    /**
     * 获得本人学校的信息
     */
    @RequestMapping("self-info")
    public String getEditInfo(HttpSession session,Model model){
        User user = (User) session.getAttribute("user");
        School schoolSelfInfo = schoolService.getSchoolInfo(user.getSchoolId());

        // 如果没有用户信息,该页面上出现添加学校列表选项
        if (schoolSelfInfo==null){
            return "eroor";
        }

        model.addAttribute("school",schoolSelfInfo);
        return "school/info";
    }

    /**
     * 通过学校id获得学校信息
     */
    @RequestMapping("info")
    public String getSchoolInfo(String id,Model model){
        School schoolInfo = schoolService.getSchoolInfo(id);
        if (schoolInfo==null){
            return "eroor";
        }
        model.addAttribute("school",schoolInfo);
        return "school/info";
    }
    /**
     * 通过学校id获得学校信息
     */
    @RequestMapping("info-test")
    @ResponseBody
    public School getSchoolInfoTest(String schoolId){
        return schoolService.getSchoolInfo(schoolId);
    }


}
