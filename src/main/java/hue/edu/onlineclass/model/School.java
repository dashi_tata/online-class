package hue.edu.onlineclass.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author dashi
 */
@TableName("tb_school")
@Data
public class School {
    /**
     * 学校id
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 学校名
     */
    @TableField("name")
    private String name;
    /**
     * 学校地址
     */
    @TableField("address")
    private String address;
    /**
     * 学校信息
     */
    @TableField("info")
    private String info;

    /**
     * 图片信息
     */
    @TableField("pic_path")
    private String picPath;
}
