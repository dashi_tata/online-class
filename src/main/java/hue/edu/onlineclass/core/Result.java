package hue.edu.onlineclass.core;

import com.alibaba.fastjson.JSON;
import lombok.Getter;

@Getter
public class Result {

    private int code;

    private String message;

    private Object data;

    public Result setCode(ResultCode code) {
        this.code=code.getCode();
        return this;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public Result setData(Object data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
