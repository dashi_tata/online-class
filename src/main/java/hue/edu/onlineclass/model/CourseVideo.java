package hue.edu.onlineclass.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dashi
 */
@Data
@NoArgsConstructor
@TableName("tb_course_video")
public class CourseVideo {

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 文件名
     */
    @TableField("name")
    private String name;
    /**
     * 课程id
     */
    @TableField("course_id")
    private String courseId;
    /**
     * 文件地址
     */
    @TableField("file_handler")
    private String fileHandler;
    /**
     * 视频封面
     */
    @TableField("pic_path")
    private String picPath;

    public CourseVideo(String name, String courseId, String fileHandler,String picPath){
        this.name = name;
        this.courseId = courseId;
        this.fileHandler = fileHandler;
        this.picPath=picPath;
    }
    public CourseVideo(String name, String courseId, String fileHandler){
        this.name = name;
        this.courseId = courseId;
        this.fileHandler = fileHandler;
    }
}
