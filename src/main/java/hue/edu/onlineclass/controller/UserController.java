package hue.edu.onlineclass.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import hue.edu.onlineclass.core.BaseController;
import hue.edu.onlineclass.core.Result;
import hue.edu.onlineclass.model.School;
import hue.edu.onlineclass.model.User;
import hue.edu.onlineclass.service.SchoolService;
import hue.edu.onlineclass.service.UserService;
import hue.edu.onlineclass.utils.PageUtils;
import hue.edu.onlineclass.utils.ResultUtils;
import hue.edu.onlineclass.vo.UserSearch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dashi
 */
@Slf4j
@Controller
public class UserController extends BaseController {

    @Autowired
    private UserService userService;
    @Autowired
    private SchoolService schoolService;


    /**
     * 获得用户列表
     */
    @RequestMapping("/user/list")
    public String getUserInfo(Model model, @ModelAttribute("search") UserSearch search, Integer pageNum){
        log.info(search.toString());
        Page page = PageUtils.defaultPage(pageNum);

        page = userService.getUserPage(search, page);
        List<School> schoolList = schoolService.getSchoolList();

        model.addAttribute("schoolList",schoolList);
        model.addAttribute("page", page);

        return "user/list";
    }

    /**
     * 通过id,获得用户信息
     */
    @RequestMapping("/user/info")
    public String getUserInfo(String id,Model model){
        User user = userService.getUserInfo(id);
        if (user==null){
            return redirect("error");
        }
        model.addAttribute("user",user);
        return "user/info";
    }

    /**
     * 打开新增页面
     */
    @RequestMapping("/user/add-page")
    public String openAddPage(String id,Model model){
//        User user = userService.getUserInfo(id);
//        if (user==null){
//            return redirect("error");
//        }
//        model.addAttribute("user",user);
        return "user/add";
    }

    /**
     * 新增用户
     */
    @ResponseBody
    @RequestMapping("/user/add")
    public Result addUser(MultipartFile file,User user){
        user.setStatus(true);
        Integer effectLine = userService.addUser(file,user);
        return ResultUtils.genResultByEffectLine(effectLine,"成功新建用户","新建用户失败");
    }

    /**
     * 打开 编辑用户 页面
     */
    @RequestMapping("/user/edit-page")
    public String openEditPage(String id,Model model){
        User user = userService.getUserInfo(id);
        model.addAttribute("user",user);
        model.addAttribute("self",false);
        return "user/edit";
    }
    /**
     *  编辑用户
     */
    @ResponseBody
    @RequestMapping("/user/edit")
    public Result editUser(User user,MultipartFile file){
        Integer effectLine = userService.updateUser(file,user);
        return ResultUtils.genResultByEffectLine(effectLine,"成功修改用户信息","修改失败,请联系管理员");
    }

    /**
     * 获得自己的信息
     */
    @RequestMapping("/self-info")
    public String getSelfUserInfo(HttpSession session, Model model){
        User user = (User) session.getAttribute("user");
        User userInfo = userService.getUserInfo(user.getId());
        model.addAttribute("self",true);
        model.addAttribute("user",userInfo);
        return "user/info";
    }

    /**
     * 删除用户
     */
    @ResponseBody
    @RequestMapping("/user/delete")
    public Result deleteUser(String id){
        User user = new User();
        user.setId(id);
        user.setStatus(false);
        Integer effectLine = userService.updateUser(null,user);
        return ResultUtils.genResultByEffectLine(effectLine,"成功删除用户信息","删除失败,请联系管理员");
    }

    /**
     * 重新启用用户
     */
    @ResponseBody
    @RequestMapping("/user/activation")
    public Result activationUser(String id){
        User user = new User();
        user.setId(id);
        user.setStatus(true);
        Integer effectLine = userService.updateUser(null,user);
        return ResultUtils.genResultByEffectLine(effectLine,"成功启用用户信息","启用失败,请联系管理员");
    }

    /**
     * 打开 编辑自己的信息 页面
     */
    @RequestMapping("edit-self-page")
    public String editSelfUserPage(HttpSession session,String id,Model model){
        User userOnline = (User) session.getAttribute("user");
        User user = userService.getUserInfo(userOnline.getId());
        model.addAttribute("user",user);
        model.addAttribute("self",true);
        return "user/edit";
    }

    /**
     * 编辑自己的信息
     */
    @ResponseBody
    @RequestMapping("edit-self")
    public Result editSelfUser(HttpSession session,User user,MultipartFile file){
        User userOnline = (User) session.getAttribute("user");
        user.setId(userOnline.getId());
        Integer effectLine = userService.updateUser(file, user);
        return ResultUtils.genResultByEffectLine(effectLine,"成功修改用户信息","修改失败,请联系管理员");
    }



    /**
     * 删除用户自己
     */
    @ResponseBody
    @RequestMapping("/user/delete-self")
    public Result deleteUserSelf(HttpSession session){
        User userOnline = (User) session.getAttribute("user");
        userOnline.setStatus(false);
        Integer effectLine = userService.updateUser(null,userOnline);
        return ResultUtils.genResultByEffectLine(effectLine,"成功删除用户信息","删除失败,请联系管理员");
    }

}
