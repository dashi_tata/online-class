package hue.edu.onlineclass.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.model.PermissionUrl;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author 梁柱
 */
@Mapper
@Deprecated
public interface PermissionUrlMapper extends BaseMapper<PermissionUrl> {

}