package hue.edu.onlineclass.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import hue.edu.onlineclass.mapper.ChooseCourseMapper;
import hue.edu.onlineclass.mapper.UserMapper;
import hue.edu.onlineclass.model.ChooseCourse;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dashi
 */
@Service
public class ChooseCourseService {
    @Resource
    private ChooseCourseMapper chooseCourseMapper;
    @Resource
    private UserMapper userMapper;

    /**
     * 教师添加学生
     * 返回未成功的学生id
     */
    public List<String> setChooseCourseByStudentId(String[] ids, String teacherId, String courseId) {
        List<String> errorIds = new ArrayList<>();
        for (String id : ids) {
            ChooseCourse chooseCourse = new ChooseCourse();
            QueryWrapper<ChooseCourse> wrapper = new QueryWrapper<>();
            wrapper.eq("teacher_id",teacherId);
            wrapper.eq("student_id",id);
            wrapper.eq("course_id",courseId);
            Integer count = chooseCourseMapper.selectCount(wrapper);
            if (count>1){
                // 已经选过该课程
                errorIds.add(id);
            }else {
                chooseCourse.setTeacherId(teacherId);
                chooseCourse.setStudentId(id);
                chooseCourse.setCourseId(courseId);
                chooseCourseMapper.insert(chooseCourse);
            }
        }
        return errorIds;
    }
    /**
     * 教师添加学生
     * 返回未成功的学生id
     */
    public List<String> getChooseStudentIds(String cId) {
        QueryWrapper<ChooseCourse> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id",cId);
        List<ChooseCourse> chooseCourses = chooseCourseMapper.selectList(queryWrapper);
        List<String> sIds = new ArrayList<>();
        for (ChooseCourse chooseCourse : chooseCourses) {
            sIds.add(chooseCourse.getStudentId());
        }
        return sIds;
    }


}
