package hue.edu.onlineclass.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hue.edu.onlineclass.model.School;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author dashi
 */
@Mapper
public interface SchoolMapper extends BaseMapper<School> {

    /**
     * 获得所有的学校名和学校id
     * @return 返回所有学校
     */
    @Select("select id,name from tb_school")
    List<School> getSchoolList();
}
